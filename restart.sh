#!/bin/sh 
read -n 1 -p "Type r for restart app (r/[a]): " AMSURE 
[ "$AMSURE" = "r" ] || exit
echo "" 1>&2
if [[ -z $(cat $PWD/server.pid) ]];
then
	java -jar target/standalone-0.0.1-SNAPSHOT.jar & echo $! > $PWD/server.pid
else
	PID=$(cat $PWD/server.pid) 
	kill -9 $PID & echo -n > $PWD/server.pid
	java -jar target/standalone-0.0.1-SNAPSHOT.jar & echo $! > $PWD/server.pid
fi
read -n 1 -p "Type smth for close window ([a]): " AMSURE 
exit