#!/bin/bash 
read -n 1 -p "Type y for start app (y/[a]): " AMSURE 
[ "$AMSURE" = "y" ] || exit
echo "" 1>&2
if [ -z $(cat $PWD/server.pid)];
then
	java -jar target/standalone-0.0.1-SNAPSHOT.jar & echo $! > $PWD/server.pid
else
	exit 1
fi