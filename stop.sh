#!/bin/sh 
read -n 1 -p "Type s for stop app (s/[a]): " AMSURE 
[ "$AMSURE" = "s" ] || exit
echo "" 1>&2
if [[ -z $(cat $PWD/server.pid) ]];
then
	exit 1
else
	PID=$(cat $PWD/server.pid) 
	kill -9 $PID & echo -n > $PWD/server.pid
fi