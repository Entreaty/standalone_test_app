# README #

### Описание задачи: ###
~~~~~~
Необходимо разработать standalone app

В базе данных должна быть одна обязательная таблица users:
* Id long
* login varchar(20)
* password varchar(10)

Обязательная часть:
* С помощью http rest запросов необходимо иметь возможность:
* Добавить нового пользователя,
* Изменить логин или пароль,
* Заблокировать пользователя,
* По логину и паролю найти пользователя для авторизации.

Бонусная часть:
* Логирование изменений объекта
* bash скрипт start, stop, restart

Основной стек технологий: java8, spring boot, H2 database in memory, maven
~~~~~~

### Подготовка к работе ###

* Выполнить команду /mvnw package или запустить build.bat в директории проекта.
* Для запуска проекта без IDEA - можно использовать bash-скрипты start.sh, stop.sh, restart.sh (server.pid - хранит информацию о pid запущенного сервера)

### Проверка запросов ###

* Создание юзера: POST http://localhost:8080/user/ параметры login и password в теле запроса.
* Изменение юзера: PUT http://localhost:8080/user/{id} параметры login и password в теле запроса.
* Блокировка юзера: PATCH http://localhost:8080/user/{id}
* Получение юзера по логину и паролю: POST http://localhost:8080/user/search параметры login и password в теле запроса.
* Получение юзера по id: GET http://localhost:8080/user/{id}
* Получение списка юзеров: GET http://localhost:8080/user/
* Получение токена: POST http://localhost:8080/token/ параметры login и password в теле запроса.