package com.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
@Table(name = "users", indexes =
		{@Index(unique = true, name = "users_login_uindex", columnList = "login"),
				@Index(name = "users_login_password_active_index", columnList = "login,password,active")})
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private long active = 1;

	@NotEmpty(message = "Please provide login")
	@Column(length = 20)
	private String login;

	@JsonIgnore
	@NotEmpty(message = "Please provide password")
	@Column(length = 10)
	private String password;

	public User() {
	}

	public User(long id, String login, String password) {
		this.id = id;
		this.login = login;
		this.password = password;
	}

	@Override
	public String toString() {
		return String.format(
				"User[id=%d, login='%s', password='%s']",
				id, login, password);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getActive() {
		return active;
	}

	public void setActive(long active) {
		this.active = active;
	}
}
