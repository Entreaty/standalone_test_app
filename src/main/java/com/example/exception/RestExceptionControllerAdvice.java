package com.example.exception;

import com.example.model.ErrorResponse;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice(annotations = RestController.class)
public class RestExceptionControllerAdvice {

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Map handle(MethodArgumentNotValidException exception) {
		return error(exception.getBindingResult().getFieldErrors()
				.stream()
				.map(FieldError::getDefaultMessage)
				.collect(Collectors.toList()));
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handle(DataIntegrityViolationException ex) {
		return createErrorResponse("org.springframework.dao.DataIntegrityViolationException",
				"This login already exists.", 400);
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handle(MethodArgumentTypeMismatchException ex) {
		return createErrorResponse("org.springframework.web.method.annotation.MethodArgumentTypeMismatchException",
				"User ID should be integer.", 400);
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handle(ConstraintViolationException ex) {
		return createErrorResponse(
				"javax.validation.ConstraintViolationException",
				ex.getConstraintViolations()
						.stream()
						.map(ConstraintViolation::getMessageTemplate)
						.collect(Collectors.toList()).get(0),
				400);
	}

	private Map error(Object message) {
		return Collections.singletonMap("error", message);
	}

	private ErrorResponse createErrorResponse(String exception, String message, int status) {
		ErrorResponse error = new ErrorResponse();
		error.setTimestamp(System.currentTimeMillis());
		error.setStatus(status);
		error.setError("Bad Request");
		error.setException(exception);
		error.setMessage(message);
		return error;
	}

}