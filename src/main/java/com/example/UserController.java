package com.example;

import com.example.model.User;
import com.example.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
public class UserController {
	private static final Logger log = LoggerFactory.getLogger(StandaloneApplication.class);

	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/user/", method = RequestMethod.POST)
	public ResponseEntity create(@RequestParam(value = "login", name = "login") String login,
								 @RequestParam(value = "password", name = "password") String password,
								 UriComponentsBuilder b) {
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		log.info("CREATED: " + String.valueOf(userRepository.saveAndFlush(user)));

		UriComponents uriComponents = b.path("/user/{id}").buildAndExpand(user.getId());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponents.toUri());
		return new ResponseEntity(headers, HttpStatus.CREATED);
	}

	@Transactional
	@RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
	public ResponseEntity update(@PathVariable("id") long id,
								 @RequestParam(value = "login", name = "login", required = false) String login,
								 @RequestParam(value = "password", name = "password", required = false) String password) {
		User user = userRepository.findOne(id);
		if (user != null) {
			if (login != null) user.setLogin(login);
			if (password != null) user.setPassword(password);
			log.info("UPDATED: " + String.valueOf(userRepository.saveAndFlush(user)));
			return new ResponseEntity(HttpStatus.OK);
		}
		return new ResponseEntity(HttpStatus.NOT_FOUND);
	}

	@Transactional
	@RequestMapping(value = "/user/{id}", method = RequestMethod.PATCH)
	public ResponseEntity block(@PathVariable("id") long id) throws Exception {
		User user = userRepository.findOne(id);
		if (user != null) {
			user.setActive(0);
			log.info("BLOCKED: " + String.valueOf(userRepository.saveAndFlush(user)));
			return new ResponseEntity(HttpStatus.OK);
		}
		return new ResponseEntity(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public User getOne(@PathVariable("id") long id) {
		return userRepository.findOne(id);
	}

	@RequestMapping(value = "/user/search", method = RequestMethod.POST)
	public User getByLoginAndPassword(@RequestParam("login") String login,
									  @RequestParam("password") String password) {
		return userRepository.findByLoginAndPasswordAndActive(login, password, 1);
	}

	@RequestMapping(value = "/user/", method = RequestMethod.GET)
	public List<User> getAll() {
		return userRepository.findAll();
	}
}
