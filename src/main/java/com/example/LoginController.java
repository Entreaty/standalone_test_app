package com.example;

import com.example.model.User;
import com.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.security.SecureRandom;

@RestController
public class LoginController {

	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/token/", method = RequestMethod.POST)
	public ResponseEntity token(@RequestParam(value = "login", name = "login") String login,
								 @RequestParam(value = "password", name = "password") String password) {
		User user = userRepository.findByLoginAndPasswordAndActive(login, password, 1);
		if (user != null) {
			// .. логика регистрации пользователя в предполагаемой системе ..
			// Например, после успешной авторизации создание и запись в бд нового токена, передача его пользователю.
			// Для использования API сервиса пользователю необходимо будет использовать полученный токен.
			SecureRandom random = new SecureRandom();
			String token = new BigInteger(130, random).toString(32);
			HttpHeaders headers = new HttpHeaders();
			headers.set("AUTHORIZATION", token);
			return new ResponseEntity(headers, HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.UNAUTHORIZED);
		}
	}
}
